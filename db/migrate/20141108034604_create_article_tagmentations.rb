class CreateArticleTagmentations < ActiveRecord::Migration
  def change
    create_table :article_tagmentations do |t|
      t.integer :article_id
      t.integer :tag_id

      t.timestamps
    end
    add_index :article_tagmentations, :article_id
    add_index :article_tagmentations, :tag_id
  end
end
