json.array!(@articles) do |article|
  json.extract! article, :id, :author, :title, :content, :publish, :slug
  json.url article_url(article, format: :json)
end
