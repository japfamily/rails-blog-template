class ArticlesController < ApplicationController
  before_action :set_article, only: [:edit, :update, :destroy]
  before_action :set_article_redirect, only: [:show]
  before_action :logged_in_user, except: [:show]
  #http_basic_authenticate_with name: ENV["BLOG_USERNAME"], password: ENV["BLOG_PASSWORD"],
  #                             except: [:show]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    unless @article.publish?
      flash[:warning] = "The requested resource is currently not available."
      redirect_to root_url
    end
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        if @article.publish?
          format.html { redirect_to @article, notice: 'Article was successfully created.' }
          format.json { render :show, status: :created, location: @article }
        else
          format.html { redirect_to articles_path, notice: 'Article was successfully created.' }
          format.json { render :show, status: :created, location: articles_path }
        end
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        if @article.publish?
          format.html { redirect_to @article, notice: 'Article was successfully updated.' }
          format.json { render :show, status: :ok, location: @article }
        else
          format.html { redirect_to articles_path, notice: 'Article was successfully updated.' }
          format.json { render :show, status: :ok, location: articles_path }
        end
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article_redirect
      @article = Article.friendly.find(params[:id])
      # If an old id or a numeric id was used to find the record, then
      # the request path will not match the article_path, and we should do
      # a 301 redirect that uses the current friendly id.     
      if request.path != article_path(@article)
        redirect_to @article, status: :moved_permanently
      end
    end

    def set_article
      @article = Article.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:author, :title, :content, :publish, :slug, :tag_ids => [] )
    end
end
