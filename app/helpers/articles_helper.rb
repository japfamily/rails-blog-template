module ArticlesHelper
  class HTMLwithPygments < Redcarpet::Render::HTML
    def block_code(code, language)
      sha = Digest::SHA1.hexdigest(code)
      Rails.cache.fetch ["code", language, sha].join("-") do
        Pygments.highlight(code, lexer: language, options: { lineanchors: "line" })
      end
    end
  end

  def markdown(text)
    options = { 
      filter_html:     true,
      no_images:       false,
      no_links:        false,
      no_styles:       false,
      escape_html:     false,
      safe_links_only: false,
      with_toc_data:   false,
      hard_wrap:       true,
      xhtml:           false,
      prettify:        false
    }
    extensions = {
      no_intra_emphasis:            true,
      tables:                       false,
      fenced_code_blocks:           true,
      autolink:                     true,
      disable_indented_code_blocks: false,
      strikethrough:                true,
      lax_spacing:                  true,  # This causes code blocks not to display??
      space_after_headers:          false,
      superscript:                  true,
      underline:                    false,
      highlight:                    false,
      quote:                        false,
      footnotes:                    false 
    }
    #renderer = Redcarpet::Render::HTML.new(options)
    renderer = HTMLwithPygments.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)
    markdown.render(text).html_safe
  end
end

