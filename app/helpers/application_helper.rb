module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "InfallibleKitty's Blog"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # Translates rails flash types to bootstrap
  def bootify(name)
    case name
    when "notice"
      return "info"
    when "error"
      return "danger"
    end
    return name
  end
end

