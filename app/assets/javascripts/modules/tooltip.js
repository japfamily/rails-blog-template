modulejs.define('tooltip', function () {

  // module setup code and definitions
  console.log('Binding tooltip and popover handlers.');

  var tooltip_obj = function () {
    $('body').popover({
        selector: '[data-toggle="popover"]'
    });

    $('body').tooltip({
        selector: 'a[rel="tooltip"], [data-toggle="tooltip"]'
    });
  };

  console.log('Binding tooltip and popover handlers complete.');
  return tooltip_obj;

});