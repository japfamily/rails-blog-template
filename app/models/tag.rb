class Tag < ActiveRecord::Base
  has_many :article_tagmentations
  has_many :articles, through: :article_tagmentations

  validates :name, presence: true, uniqueness: { case_senstiive: false }
end
