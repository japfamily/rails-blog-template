class Article < ActiveRecord::Base
  has_many :article_tagmentations
  has_many :tags, through: :article_tagmentations

  default_scope -> { order('created_at DESC') }

  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]

  validates_presence_of :title, :slug

  def should_generate_new_friendly_id?
    title_changed?
  end
end
